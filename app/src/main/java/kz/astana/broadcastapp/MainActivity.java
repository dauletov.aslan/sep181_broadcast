package kz.astana.broadcastapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private final String CHANNEL_ID = "CHANNEL_ID";
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();

        Button sendBroadcast = findViewById(R.id.sendBroadcast);
        sendBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("kz.astana.broadcastapp.SOME_ACTION");
                sendBroadcast(intent);
            }
        });

        Button registerBroadcast = findViewById(R.id.registerBroadcast);
        registerBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerBroadcast();
            }
        });

        Button sendBroadcastDynamic = findViewById(R.id.sendBroadcastDynamic);
        sendBroadcastDynamic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("kz.astana.ACTION");
                intent.putExtra("kz.astana.MESSAGE", "Hello");
                sendBroadcast(intent);
            }
        });

        Button startService = findViewById(R.id.startService);
        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                startService(intent);
            }
        });

        Button sendNotification = findViewById(R.id.sendNotification);
        sendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String replyLabel = "Enter your reply here";
                RemoteInput remoteInput =
                        new RemoteInput.Builder("KEY_TEXT_REPLY")
                                .setLabel(replyLabel)
                                .build();

                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        MainActivity.this,
                        100,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

                Notification.Action replyAction =
                        new Notification.Action.Builder(
                                R.drawable.ic_launcher_foreground,
                                "Reply",
                                pendingIntent
                        )
                                .addRemoteInput(remoteInput)
                                .build();

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    Notification notification =
                            new Notification.Builder(MainActivity.this, CHANNEL_ID)
                                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                                    .setContentTitle("New message")
                                    .setContentText("Hello my friend!")
                                    .setAutoCancel(true)
                                    .addAction(replyAction)
                                    .build();

                    notificationManager.notify(1, notification);
                }
            }
        });
    }

    private void registerBroadcast() {
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra("kz.astana.MESSAGE");
                Log.d("Hello", s);
                Toast.makeText(context, s, Toast.LENGTH_LONG).show();
            }
        };
        IntentFilter intentFilter = new IntentFilter("kz.astana.ACTION");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Notification channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.setDescription("My notification channel");
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}